        $(function(){
            $('#my-modal').on('show.bs.modal', function(e){
                console.log('Se esta mostrando el modal');
                $('#btnComment').removeClass('btn-success');
                $('#btnComment').addClass('btn-danger');
                $('#btnComment').prop('disabled', true);
                
                
            });
            $('#my-modal').on('shown.bs.modal', function(e){
                console.log('Se mostrò el modal')
            });
            $('#my-modal').on('hide.bs.modal', function(e){
                console.log('Se esta ocultando el modal');
                $('#btnComment').removeClass('btn-danger');
                $('#btnComment').addClass('btn-success');
                $('#btnComment').prop('disabled', false);
                
            });
            $('#my-modal').on('hidden.bs.modal', function(e){
                console.log('Se ocultò el modal')
            });
        });